from django.db import models

from web.domain.discount import Discount
from web.domain.order import Order


class DiscountUsageHistory(models.Model):
    order = models.ForeignKey(Order, null=True, on_delete=models.SET_NULL)
    discount = models.ForeignKey(Discount, null=True, on_delete=models.SET_NULL)
    createdOnUtc = models.DateTimeField(auto_now_add=True)
