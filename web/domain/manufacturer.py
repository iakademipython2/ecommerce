from django.db import models


class Manufacturer(models.Model):
    name = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=500, null=True)
    deleted = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
