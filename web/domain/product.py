from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=500, null=True)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
    isShipEnabled = models.BooleanField(default=False)
    isFreeShipping = models.BooleanField(default=False)
    stockQuantity = models.IntegerField(default=0)
    displayStockQuantity = models.BooleanField(default=False)
    price = models.FloatField(null=True)
    oldPrice = models.FloatField(null=True)
    productCost = models.FloatField(null=True)
    markAsNew = models.BooleanField(default=False)
    markAsNewStartDateTimeUtc = models.DateTimeField(auto_now_add=True, null=True)
    markAsNewEndDateTimeUtc = models.DateTimeField(auto_now_add=False, null=True)
    markAsNewEndDateTimeUtc = models.DateTimeField(auto_now_add=False, null=True)
    weight = models.FloatField(null=True)
    length = models.FloatField(null=True)
    width = models.FloatField(null=True)
    height = models.FloatField(null=True)
    deleted = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
