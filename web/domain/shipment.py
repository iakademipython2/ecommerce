from django.db import models

from web.domain.order import Order


class Shipment(models.Model):
    order = models.ForeignKey(Order, null=True, on_delete=models.SET_NULL)
    trackingNumber = models.IntegerField(null=True)
    totalWeight = models.FloatField(null=True)
    shippedDateUtc = models.DateTimeField(null=True)
    deliveryDateUtc = models.DateTimeField(null=True)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
    