from django.db import models

from web.domain.discount import Discount
from web.domain.product import Product


class DiscountAppliedToProductsMapping(models.Model):
    discount = models.ForeignKey(Discount, null=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
