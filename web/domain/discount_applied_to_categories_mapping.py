from django.db import models

from web.domain.category import Category
from web.domain.discount import Discount


class DiscountAppliedToCategoriesMapping(models.Model):
    discount = models.ForeignKey(Discount, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
