from django.db import models


class Address(models.Model):
    firstName = models.CharField(max_length=50, null=True)
    lastName = models.CharField(max_length=50, null=True)
    email = models.CharField(max_length=100, null=True)
    company = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=20, null=True)
    description = models.CharField(max_length=200, null=True)
