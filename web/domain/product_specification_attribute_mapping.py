from django.db import models

from web.domain.product import Product
from web.domain.specification_attribute_option import SpecificationAttributeOption


class ProductSpecificationAttributeMapping(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    specificationAttributeOption = models.ForeignKey(SpecificationAttributeOption, null=True, on_delete=models.SET_NULL)
    showOnProductPage = models.BooleanField(default=False)
