from django.db import models

from web.domain.product import Product
from web.domain.product_tag import ProductTag


class ProductProductTagMapping(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    productTag = models.ForeignKey(ProductTag, null=True, on_delete=models.SET_NULL)
