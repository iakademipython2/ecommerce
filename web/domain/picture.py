from django.db import models


class Picture(models.Model):
    mimeType = models.CharField(max_length=20, null=True)
    seoFileName = models.CharField(max_length=50, null=True)
    altAttribute = models.CharField(max_length=200, null=True)
    titleAttribute = models.CharField(max_length=200, null=True)
    isNew = models.BooleanField(default=False)
    image = models.ImageField(null=True)
