from django.db import models

from web.domain.category import Category
from web.domain.product import Product


class ProductCategoryMapping(models.Model):
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)

