from django.db import models

from web.domain.product import Product
from web.domain.product_attribute import ProductAttribute


class ProductProductAttributeMapping(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    productAttribute = models.ForeignKey(ProductAttribute, null=True, on_delete=models.SET_NULL)
    isRequired = models.BooleanField(default=False)
