from django.db import models

from web.domain.product import Product


class ProductStockQuantityHistory(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    stockQuantity = models.IntegerField(default=0)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
