from django.db import models

from web.domain.discount import Discount
from web.domain.manufacturer import Manufacturer


class DiscountAppliedToManufacturersMapping(models.Model):
    discount = models.ForeignKey(Discount, null=True, on_delete=models.SET_NULL)
    manufacturer = models.ForeignKey(Manufacturer, null=True, on_delete=models.SET_NULL)
