from django.db import models

from web.domain.picture import Picture
from web.domain.product import Product


class ProductPictureMapping(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    picture = models.ForeignKey(Picture, null=True, on_delete=models.SET_NULL)
