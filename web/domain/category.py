from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=500, null=True)
    deleted = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
    parentCategoryId = models.IntegerField(default=0)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
