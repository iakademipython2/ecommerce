from django.db import models


class OrderNote(models.Model):
    note = models.CharField(max_length=400, null=True)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
