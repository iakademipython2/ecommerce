from django.db import models

from web.domain.manufacturer import Manufacturer
from web.domain.product import Product


class ProductManufacturerMapping(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    manufacturer = models.ForeignKey(Manufacturer, null=True, on_delete=models.SET_NULL)
