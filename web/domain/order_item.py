from django.db import models

from web.domain.order import Order
from web.domain.product import Product


class OrderItem(models.Model):
    orderItemCode = models.CharField(max_length=50, null=True)
    order = models.ForeignKey(Order, null=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    quantity = models.IntegerField(default=0)
    unitPriceIncludeTax = models.FloatField(null=True)
    unitPriceExcludeTax = models.FloatField(null=True)
    discountAmountIncludeTax = models.FloatField(null=True)
    discountAmountExcludeTax = models.FloatField(null=True)
    originalProductPrice = models.FloatField(null=True)
