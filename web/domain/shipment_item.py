from django.db import models

from web.domain.order_item import OrderItem
from web.domain.shipment import Shipment


class ShipmentItem(models.Model):
    shipment = models.ForeignKey(Shipment, null=True, on_delete=models.SET_NULL)
    orderItem = models.ForeignKey(OrderItem, null=True, on_delete=models.SET_NULL)
    quantity = models.IntegerField(null=True)

