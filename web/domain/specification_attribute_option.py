from django.db import models

from web.domain.specification_attribute import SpecificationAttribute


class SpecificationAttributeOption(models.Model):
    specificationAttribute = models.ForeignKey(SpecificationAttribute, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=200, null=True)
