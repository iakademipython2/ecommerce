from django.db import models


class ProductTag(models.Model):
    name = models.CharField(max_length=200, null=True)
