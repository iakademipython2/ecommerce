from django.db import models


class Discount(models.Model):
    name = models.CharField(max_length=200, null=True)
    usePercentage = models.BooleanField(default=False)
    discountPercentage = models.FloatField(null=True)
    discountAmount = models.FloatField(null=True)
    maximumDiscountAmount = models.FloatField(null=True)
    startDateTimeUtc = models.DateTimeField(auto_now_add=True, null=True)
    endDateTimeUtc = models.DateTimeField(auto_now_add=False, null=True)
    requiresCouponCode = models.BooleanField(default=False)
    couponCode = models.CharField(max_length=200, null=True)
    limitationTimes = models.IntegerField(default=0)
    appliedToSubCategories = models.BooleanField(default=False)

