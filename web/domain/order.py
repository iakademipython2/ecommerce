from django.db import models

from web.domain.address import Address
from web.domain.customer import Customer


class Order(models.Model):
    orderCode = models.CharField(max_length=50, null=True)
    customer = models.ForeignKey(Customer, null=True, on_delete=models.SET_NULL)
    address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL)
    orderStatusId = models.IntegerField(default=0, null=True)
    paymentStatusId = models.IntegerField(default=0, null=True)
    paymentMethodSystemName = models.CharField(max_length=200, null=True)
    customerCurrencyCode = models.CharField(max_length=20, null=True)
    orderTax = models.FloatField(null=True)
    orderDiscount = models.FloatField(null=True)
    orderTotal = models.FloatField(null=True)
