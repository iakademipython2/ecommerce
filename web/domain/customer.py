from django.db import models


class Customer(models.Model):
    customerIdentity = models.CharField(max_length=200, null=True)
    username = models.CharField(max_length=200, null=True)
    firstName =  models.CharField(max_length=50,null=True)
    lastName = models.CharField(max_length=50, null=True)
    email = models.CharField(max_length=75, null=True)
    active = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    lastIpAddress = models.CharField(max_length=20, null=True)
    lastLoginDateUtc = models.DateTimeField(auto_now_add=True, null=True)
    lastActivityDateUtc = models.DateTimeField(null=True)
