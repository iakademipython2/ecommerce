from django.db import models

from web.domain.customer import Customer
from web.domain.product import Product


class ShoppingCartItem(models.Model):
    customer = models.ForeignKey(Customer, null=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    quantity = models.IntegerField(default=0)
    createdOnUtc = models.DateTimeField(auto_now_add=True, null=True)
