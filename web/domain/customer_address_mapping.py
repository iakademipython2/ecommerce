from django.db import models

from web.domain.address import Address
from web.domain.customer import Customer


class CustomerAddressMapping(models.Model):
    customer = models.ForeignKey(Customer, null=True, on_delete=models.SET_NULL)
    address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL)
