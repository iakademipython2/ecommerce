from django.db import models

from web.domain.product_productattribute_mapping import ProductProductAttributeMapping


class ProductAttributeValue(models.Model):
    productProductAttributeMapping = models.ForeignKey(ProductProductAttributeMapping,
                                                       null=True,
                                                       on_delete=models.SET_NULL)
    name = models.CharField(max_length=200, null=True)
    quantity = models.IntegerField(default=0)
