from web.data.data_access.data_access_category import DataAccessCategory
from web.service.iservice import IService


class CategoryService(IService):

    dataAccessCategory = None

    def __init__(self) -> None:
        super().__init__()
        global dataAccessCategory
        dataAccessCategory = DataAccessCategory()

    def insert(self, entityViewModel):
        pass

    def update(self, entityViewModel):
        pass

    def delete(self, entityViewModel):
        pass

    def getList(self):
        pass

    def getById(self, Id):
        pass