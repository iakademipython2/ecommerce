from abc import ABC, abstractmethod


class IService(ABC):

    @abstractmethod
    def insert(self, entityViewModel):
        pass

    @abstractmethod
    def update(self, entityViewModel):
        pass

    @abstractmethod
    def delete(self, entityViewModel):
        pass

    @abstractmethod
    def getList(self):
        pass

    @abstractmethod
    def getById(self, Id):
        pass
