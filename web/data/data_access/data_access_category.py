from web.data.repository.irepository import IRepository
from web.domain.category import Category


class DataAccessCategory(IRepository):

    def insert(self, entity):
        try:
            result = Category.objects.create(
                name=entity.name,
                description=entity.description,
                deleted=entity.deleted,
                published=entity.published,
                parentCategoryId=entity.parentCategoryId
            )
            if isinstance(result, Category):
                return True
            else:
                return False
        except Exception as exception:
            return exception

    def update(self, entity):
        try:
            category = Category.objects.get(id=entity.id)
            category.name = entity.name
            category.description = entity.description
            category.deleted = entity.deleted
            category.published = entity.published
            category.parentCategoryId = entity.parentCategoryId
            result = Category.objects.update(category, category.id)
            if isinstance(result, Category):
                return True
            else:
                return False
        except Exception as exception:
            return exception

    def delete(self, entity):
        try:
            category = Category.objects.get(id=entity.id)
            category.deleted = entity.deleted
            result = Category.objects.update(category, category.id)
            if isinstance(result, Category):
                return True
            else:
                return False
        except Exception as exception:
            return exception

    def getList(self):
        try:
            return Category.objects.all()
        except Exception as exception:
            return exception

    def getById(self, Id):
        try:
            return Category.objects.get(id=Id)
        except Exception as exception:
            return exception
